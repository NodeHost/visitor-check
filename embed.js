var visitorcheck_url_report="https://tool.nodehost.cloud/visitorcheck/?action=report";
var visitorcheck_url_get_question="https://tool.nodehost.cloud/visitorcheck/?action=question";
var visitorcheck_url_get_key="https://tool.nodehost.cloud/visitorcheck/?action=key";
var visitorcheck_url_get_picked="https://tool.nodehost.cloud/visitorcheck/?action=picked";
var visitorcheck_key="";
var visitorcheck_timestamp_startup=new Date().getTime();
var visitorcheck_timestamp_loaded=0;
var visitorcheck_timestamp_submit=0;
var visitorcheck_tries=0;
var visitorcheck_finished=false;

Element.prototype.remove = function(){
	this.parentElement.removeChild(this);
}
NodeList.prototype.remove = HTMLCollection.prototype.remove = function(){
		for(var i = this.length - 1; i >= 0; i--) {
				if(this[i] && this[i].parentElement) {
						this[i].parentElement.removeChild(this[i]);
				}
		}
}

//Focus events
window.addEventListener('focus', function(e){
  visitorcheck_timestamp_startup=new Date().getTime();
	if (visitorcheck_finished==false){
		nh_visitorcheck_restart();
	}
});

window.addEventListener('blur', function(e){
  visitorcheck_timestamp_startup=new Date().getTime();
});

//Startup
setTimeout(function(){
  var cssdata="";
  cssdata=cssdata + "#nhvc_loading{ font-size:15px;font-weight:700; } ";
  cssdata=cssdata + "#nhvc_loading.hidden{ display:none; } ";
  cssdata=cssdata + "#nhvc_inputbox_case{ text-align:center;color: rgb(88,88,88);border-radius:10px;background:rgb(241, 241, 241);padding:10px;margin:10px;font-weight:600;max-width:400px;font-smooth: always;font-family: \"Open Sans\", Helvetica, Arial, sans-serif !important; -webkit-font-smoothing: antialiased; } ";
	cssdata=cssdata + "#nhvc_inputbox_case:hover{ box-shadow: 0 4px 10px rgba(0, 0, 0, .08);-webkit-box-shadow: 0 4px 10px rgba(0, 0, 0, .08);-moz-box-shadow: 0 4px 10px rgba(0, 0, 0, .08); } ";
  cssdata=cssdata + "#nhvc_inputbox_case .title{ font-size:20px;font-weight:700;margin-bottom:5px;color:rgb(29, 29, 29); } ";
  cssdata=cssdata + "#nhvc_inputbox_case .description{ color:rgb(75, 75, 75);margin-bottom:10px;font-size:14px; } ";
  cssdata=cssdata + "#nhvc_inputbox_case .selectcase{ } ";
  cssdata=cssdata + "#nhvc_inputbox_case .selectcase.hidden{ display:none; } ";
  cssdata=cssdata + "#nhvc_inputbox_case .loadingcase{ } ";
  cssdata=cssdata + "#nhvc_inputbox_case .loadingcase.hidden{ display:none; } ";
  cssdata=cssdata + "#nhvc_inputbox_case .questioncase{ display:inline-block;width:20%; } ";
  cssdata=cssdata + "#nhvc_inputbox_case .question{ text-align:center;display:inline-block; } ";
  cssdata=cssdata + "#nhvc_inputbox_case .question img{ border-radius:5px;width:70%;max-width:60px;border:2px solid rgba(84, 84, 84, 1);box-shadow: 0 0 8px rgba(0, 0, 0, .06);-webkit-box-shadow: 0 0 8px rgba(0, 0, 0, .06);-moz-box-shadow: 0 0 8px rgba(0, 0, 0, .06); } ";
  cssdata=cssdata + "#nhvc_inputbox_case .question img:hover{  } ";
  cssdata=cssdata + "#nhvc_inputbox_case .optioncase{ width:80%;display:inline-block; } ";
  cssdata=cssdata + "#nhvc_inputbox_case .option{ text-align:center;display:inline-block;width:20%; } ";
  cssdata=cssdata + "#nhvc_inputbox_case .option img{ cursor: pointer;border-radius:70px;width:70%;max-width:60px;border:2px solid rgba(87, 95, 255, 1); } ";
  cssdata=cssdata + "#nhvc_inputbox_case .option img:hover{ border-radius:70px;border:2px solid rgba(92, 92, 92, 1);box-shadow: 0 0 8px rgba(0, 0, 0, .06);-webkit-box-shadow: 0 0 8px rgba(0, 0, 0, .06);-moz-box-shadow: 0 0 8px rgba(0, 0, 0, .06); } ";
  cssdata=cssdata + "#nhvc_inputbox_case .poweredby{ margin-top:10px;color:rgb(147, 147, 147);font-weight:400;font-size:10px;text-align:center; } ";
  cssdata=cssdata + "#nhvc_inputbox_case.worked{ background:rgb(201, 252, 206); } ";
  cssdata=cssdata + "#nhvc_inputbox_case.worked .title{ display:none; } ";
  cssdata=cssdata + "#nhvc_inputbox_case.worked .description{ display:none; } ";
  cssdata=cssdata + "#nhvc_inputbox_case .spinning_loader{ margin: 0px auto;width: 56px;height: 56px;box-sizing: border-box;border: solid 3px transparent;border-top-color: rgb(44,57,100);border-left-color: rgb(44,57,100);border-radius: 50%;-webkit-animation: anamation-spinner 400ms linear infinite;animation: anamation-spinner 400ms linear infinite; } ";
	cssdata=cssdata + "@-webkit-keyframes anamation-spinner{ 0% { -webkit-transform: rotate(0deg); } 100% {	-webkit-transform: rotate(360deg); }} ";
  cssdata=cssdata + "@keyframes anamation-spinner { 0% { transform: rotate(0deg); } 100% { transform: rotate(360deg); }} ";


	var css = document.createElement("style");
	css.type = "text/css";
	css.innerHTML = cssdata;
	document.body.appendChild(css);

	if (document.getElementById("nhvc_wjfo9i32")){
		//Create elements and make requests

    var content = document.createElement("div");
  	content.id="nhvc_loading";
    content.innerText="Loading captcha test...";
    document.getElementById("nhvc_wjfo9i32").appendChild(content);

    var script = document.createElement('script');
  	script.src = visitorcheck_url_get_question;
  	document.body.appendChild(script);
	}

}, 500);

//Load question data into page
function nh_visitorcheck_loadquestion(data){
  var key=data["key"];

  //Print key for developer use
  var input = document.createElement("input");
  input.setAttribute("type", "hidden");
  input.setAttribute("name", "nhvc_key");
  input.setAttribute("value", key);
  document.getElementById("nhvc_wjfo9i32").appendChild(input);

  //Generate box
	if (document.getElementById("nhvc_inputbox_case")==null){
		var content = document.createElement("div");
		content.id="nhvc_inputbox_case";
	  document.getElementById("nhvc_wjfo9i32").appendChild(content);
	}

  //Hide loading
  document.getElementById("nhvc_loading").classList.add('hidden');

  var thetest="<div class='title'>Verification</div><div class='description'>Select the item that matches the item on the left</div>";

  thetest=thetest+"<div id='nhvc_loadingcase' class='loadingcase'></div>";
  thetest=thetest+"<div id='nhvc_selectcase' class='selectcase'>";
  thetest=thetest+"<div class='questioncase'><div class='question'><img src='" + data["question"] + "'></div></div>";
  thetest=thetest+"<div class='optioncase'>";
  thetest=thetest+"<div class='option'><img src='" + data["choice_1_image"] + "' onclick=\"javascript:nh_visitorcheck_sendresult('" + key + "','" + data["choice_1_key"] + "');\"></div>";
  thetest=thetest+"<div class='option'><img src='" + data["choice_2_image"] + "' onclick=\"javascript:nh_visitorcheck_sendresult('" + key + "','" + data["choice_2_key"] + "');\"></div>";
  thetest=thetest+"<div class='option'><img src='" + data["choice_3_image"] + "' onclick=\"javascript:nh_visitorcheck_sendresult('" + key + "','" + data["choice_3_key"] + "');\"></div>";
  thetest=thetest+"<div class='option'><img src='" + data["choice_4_image"] + "' onclick=\"javascript:nh_visitorcheck_sendresult('" + key + "','" + data["choice_4_key"] + "');\"></div>";
  thetest=thetest+"<div class='option'><img src='" + data["choice_5_image"] + "' onclick=\"javascript:nh_visitorcheck_sendresult('" + key + "','" + data["choice_5_key"] + "');\"></div>";
  thetest=thetest+"</div>";
  thetest=thetest+"</div>";

  document.getElementById("nhvc_inputbox_case").innerHTML=thetest;
}

function nh_visitorcheck_sendresult(key,choice){
	visitorcheck_tries=visitorcheck_tries+1;
	visitorcheck_timestamp_submit=new Date().getTime();
  nh_vistorcheck_loading_show();
  nh_visitorcheck_getdata("" + visitorcheck_url_get_picked + "&key=" + key + "&option=" + choice + "&tries=" + visitorcheck_tries + "&time_start=" + visitorcheck_timestamp_startup + "&time_send=" + visitorcheck_timestamp_submit + "",function(data){
    if (data=="error"){
      nh_visitorcheck_restart();
    }else{
      document.getElementById("nhvc_selectcase").innerHTML="Hey look at that you are human<div class='poweredby'>Visitor Check by NodeHost</div>";

      document.getElementById("nhvc_inputbox_case").classList.add('worked');
      nh_vistorcheck_loading_hide();
			visitorcheck_finished=true;
    }
  });
}

function nh_visitorcheck_restart(){
  document.getElementById("nhvc_wjfo9i32").innerHTML="";

  var content = document.createElement("div");
  content.id="nhvc_loading";
  content.innerText="Loading captcha test...";
  document.getElementById("nhvc_wjfo9i32").appendChild(content);

  var script = document.createElement('script');
	script.src = visitorcheck_url_get_question;
	document.body.appendChild(script);
}

function nh_vistorcheck_loading_show(){
  document.getElementById("nhvc_loadingcase").innerHTML="<div class='spinning_loader'></div>";
  document.getElementById("nhvc_selectcase").classList.add('hidden');
  document.getElementById("nhvc_loadingcase").classList.remove('hidden');
}

function nh_vistorcheck_loading_hide(){
  document.getElementById("nhvc_selectcase").classList.remove('hidden');
  document.getElementById("nhvc_loadingcase").classList.add('hidden');
}

function nh_visitorcheck_getdata(url,callback){
  var xhttp = new XMLHttpRequest();
 	xhttp.withCredentials = true
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState === 4) {
      callback(xhttp.response);
    }
  }

	xhttp.open("GET", url, true);
	xhttp.send();
}
