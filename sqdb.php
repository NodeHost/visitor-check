<?php
// (C) 2014 Anthony Rossbach

$sqdb=array();

//####################################################################################################################
//####################################################################################################################-- Connection To Database
//####################################################################################################################

$sqdb["default"]=array();
$sqdb["default"]["db_server"]="localhost";
$sqdb["default"]["db_username"]="username";
$sqdb["default"]["db_password"]="password";
$sqdb["default"]["db_database"]="database";

$connections=array();

function sqdb_connect_table($table){
	global $connections;
	global $sqdb;
	if (!isset($connections["".$table.""])){
		$connections["".$table.""] = new mysqli($sqdb["".$table.""]["db_server"], $sqdb["".$table.""]["db_username"], $sqdb["".$table.""]["db_password"], $sqdb["".$table.""]["db_database"]);
		if ($connections["".$table.""]->connect_error) {
			echo $connections["".$table.""]->connect_error;
			die("We are unable to conect to our backend systems. Try again in a few minutes, we may be under heavy load.");
		}
	}
}

//####################################################################################################################
//####################################################################################################################-- SQBD -> Query
//####################################################################################################################

function sqdb_query($query,$table="default"){
	global $connections;
	if (!isset($connections["".$table.""])){ sqdb_connect_table($table); }
	$return=false;

	$return = $connections["".$table.""]->query($query);
	return $return;
}

//####################################################################################################################
//####################################################################################################################-- SQBD -> Num -> Rows
//####################################################################################################################

function sqdb_num_rows($query,$table="default"){
	global $connections;
	if (!isset($connections["".$table.""])){ sqdb_connect_table($table); }
	return $query->num_rows;
}

//####################################################################################################################
//####################################################################################################################-- SQBD -> Fetch -> Array
//####################################################################################################################

function sqdb_fetch_array($query,$table="default"){
	global $connections;
	if (!isset($connections["".$table.""])){ sqdb_connect_table($table); }
	return $query->fetch_assoc();
}

//####################################################################################################################
//####################################################################################################################-- SQBD -> Insert -> ID
//####################################################################################################################

function sqdb_insert_id($table="default"){
	global $connections;
	if (!isset($connections["".$table.""])){ sqdb_connect_table($table); }
	return $connections["".$table.""]->insert_id;
}

//####################################################################################################################
//####################################################################################################################-- SQBD -> Close
//####################################################################################################################

function sqdb_close($table="default"){
	global $connections;
	if (!isset($connections["".$table.""])){ sqdb_connect_table($table); }
	$connections["".$table.""]->close();
}
