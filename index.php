<?php

function makesafe($d,$type="basic"){
	$d = str_replace("\t","~~",$d);
	$d = str_replace("\r","",$d);
	$d = str_replace("\n","  ",$d);
	$d = str_replace("|","&#124;",$d);
	$d = str_replace("\\","&#92;",$d);
	$d = str_replace("(c)","&#169;",$d);
	$d = str_replace("(r)","&#174;",$d);
	$d = str_replace("\"","&#34;",$d);
	$d = str_replace("'","&#39;",$d);
	$d = str_replace("<","&#60;",$d);
	$d = str_replace(">","&#62;",$d);
	$d = str_replace("`","&#96;",$d);
	$d = str_replace("DELETE FROM","",$d);
	return $d;
}

function gettrueip(){
	$ip="0.0.0.0";

	if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])){ if (filter_var($_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP)){ if ($ip=="0.0.0.0"){ $ip=$_SERVER['HTTP_X_FORWARDED_FOR']; }}}
	if (isset($_SERVER['HTTP_CF_CONNECTING_IP'])){ if (filter_var($_SERVER['HTTP_CF_CONNECTING_IP'], FILTER_VALIDATE_IP)){ if ($ip=="0.0.0.0"){ $ip=$_SERVER['HTTP_CF_CONNECTING_IP']; }}}
	if (isset($_SERVER['Cf-Connecting-IP'])){ if (filter_var($_SERVER['Cf-Connecting-IP'], FILTER_VALIDATE_IP)){ if ($ip=="0.0.0.0"){ $ip=$_SERVER['Cf-Connecting-IP']; }}}
	if (isset($_SERVER['HTTP_CLIENT_IP'])){ if (filter_var($_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP)){ if ($ip=="0.0.0.0"){ $ip=$_SERVER['HTTP_CLIENT_IP']; }}}
	if (isset($_SERVER['REMOTE_ADDR'])){ if (filter_var($_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP)){ if ($ip=="0.0.0.0"){ $ip=$_SERVER['REMOTE_ADDR']; }}}

	return $ip;
}

function codegenerate($length,$type){
	if ($length<=0){
	$length=10;
	}
	if ($type=="normal"){
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
	}
	if ($type=="password"){
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_-_-_-_$$%%#@@!!((*&&^^%%@&^$T^(!++++';
	}
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, strlen($characters) - 1)];
	}
	return $randomString;
}

//Making a image for each item random with random sizes and quality to confuse signatures
function system_scaleimage_random($image,$clean){
	$num=time();
	$target_path=$image;
	$maxsize=mt_rand(55,120);
	$quality=mt_rand(60,100);

	if ($clean==true){
		$maxsize=140;
		$quality=90;
	}

	if(file_exists($target_path)) {
		$realimage=false;
		if (filesize($target_path) > 11){ //--Must check as images lower then 11 bytes dont give image type data
			if (exif_imagetype($target_path)==IMAGETYPE_GIF){ $im=imagecreatefromgif($target_path); $realimage=true; }
			if (exif_imagetype($target_path)==IMAGETYPE_JPEG){ $im=imagecreatefromjpeg($target_path); $realimage=true; }
			if (exif_imagetype($target_path)==IMAGETYPE_PNG){ $im=imagecreatefrompng($target_path); $realimage=true; }
		}
		if ($realimage==false){
			return "0";
			unlink ($target_path);
		}else{
			list($width, $height)=getimagesize($target_path);

			//scale
			if ($width > $maxsize){ $newwidth=$maxsize; $math=$width/$maxsize; $newheight=$height/$math; }else{ $newwidth=$width; $newheight=$height; }
			if ($newheight > $maxsize){ $newheightr=$maxsize; $math=$newheight/$newheightr; $newheight=$newheightr; $newwidth=$newwidth/$math; }
			$tn=imagecreatetruecolor($newwidth, $newheight);

			if ($clean==false){
				$backgroundColor1=imagecolorallocate($tn, 245,247,255); imagefill($tn, 0, 0, $backgroundColor1);
			}else{
				$backgroundColor1=imagecolorallocate($tn, 255,255,255); imagefill($tn, 0, 0, $backgroundColor1);
			}
			imagecopyresampled($tn, $im, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

			//Rotate
			if ($clean==false){
				$transColor = imagecolorallocatealpha($tn, 245,247,255, 127);
				$tn = imagerotate($tn, mt_rand(-90,90), $transColor);
				imagealphablending($tn, false);
    		imagesavealpha($tn, true);
			}

			$width  = imagesx($tn);
			$height = imagesy($tn);

			//Lines
			if ($clean==false){
				for($i=0;$i<mt_rand(7,20);$i++) {
					$line_color = imagecolorallocate($tn, mt_rand(0,255),mt_rand(0,255),mt_rand(0,255));
				  imageline($tn,0,mt_rand()%$width,$width,mt_rand()%$width,$line_color);
				}
			}

			//Dots
			if ($clean==false){
				for($i=0;$i<mt_rand(90,400);$i++) {
					$pixel_color = imagecolorallocate($tn, mt_rand(0,255),mt_rand(0,255),mt_rand(0,255));
				  imagesetpixel($tn,mt_rand()%$width,mt_rand()%$width,$pixel_color);
				}
			}

			ob_start();
			imagejpeg($tn, NULL, $quality);
			imagedestroy($tn);
			$i = ob_get_clean();
			$base64 = 'data:image/jpeg;base64,' . base64_encode($i);

			return $base64;
		}
	}else{
		return false;
	}
}

function random_pic($dir = 'images'){
    $files = glob($dir . '/*.*');
    $file = array_rand($files);
    return $files[$file];
}

function bot_detected(){
	return (isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/bot|crawl|slurp|spider|mediapartners/i', $_SERVER['HTTP_USER_AGENT']));
}

include('sqdb.php');

$origin=$_SERVER['HTTP_ORIGIN'];
header('Access-Control-Allow-Origin: ' . $origin);
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
set_time_limit(90);
ignore_user_abort(true);
ini_set('output_buffering', 0);
ini_set('display_errors', '0');
ini_set('date.timezone', 'America/New_York');
ini_set('memory_limit','32M');
ob_start ("ob_gzhandler");

//Updated to use the hostname but if that does not work replace $_SERVER['HOST_NAME'] with the domain this is hosted on like tool.nodehost.cloud
setcookie('hf34',codegenerate("10","normal"),time()+3600, "/", $_SERVER['HOST_NAME'], 1);

if (isset($_GET["key"])){
	$session_key=makesafe($_GET["key"]);
}else{
	$session_key=false;
}

if (isset($_GET["option"])){
	$session_option=makesafe($_GET["option"]);
}else{
	$session_option=false;
}

$session_userip=gettrueip();

//Geneate a key
if ($_GET["action"]=="key"){
	$key=codegenerate("60","normal");
	$date=time();
	$resulta=sqdb_query("INSERT INTO attempts(id_long, date) VALUES('$key', '$date')");
	echo $key;
}

if ($_GET["action"]=="report"){
	if ($session_key!=false){
		$queryx=sqdb_query("SELECT * FROM attempts WHERE id_long='$session_key' AND usable='1' LIMIT 1");
		if (!sqdb_num_rows($queryx)==0){
			while ($row=sqdb_fetch_array($queryx)){
				$updatep=sqdb_query("UPDATE attempts SET usable='0' WHERE id_long='$session_key' LIMIT 1");
				echo $row["score"];
			}
		}else{
			echo 0;
		}
	}else{
		echo 0;
	}
}

if ($_GET["action"]=="picked"){
	$dateremove=time()-(86400*7); //--7 days
	$deleteresult_p=sqdb_query("DELETE FROM puzzle WHERE date<='$dateremove' LIMIT 100");
	$deleteresult_a=sqdb_query("DELETE FROM attempts WHERE date<='$dateremove' LIMIT 100");
	header('Content-Type: text/plain');

	if ($session_key!=false){
		if ($session_option!=false){

			$queryx=sqdb_query("SELECT * FROM puzzle WHERE id_long='$session_key' AND solution='$session_option' LIMIT 1");
			if (!sqdb_num_rows($queryx)==0){

				$limitifreal=sqdb_query("SELECT * FROM attempts WHERE id_long='$session_key' AND usable='1' AND ip='$session_userip' LIMIT 1");
				if (!sqdb_num_rows($limitifreal)==0){

					//Passed so lets update score
					$score=60;

					//get timing to use as rules
					$time_start=intval($_GET["time_start"]);
					$time_send=intval($_GET["time_send"]);

					//Set vars we use
					$time_fromload=$time_send-$time_start;

					$botuseragent=bot_detected();
					if ($botuseragent==false){
						$score=$score+13;
					}

					if (isset($_COOKIE['hf34'])){
						$score=$score+4;
					}

					//did it only take one try?
					if ($_GET["tries"]=="1"){
						$score=$score+5;
					}else{
						$score=$score-($_GET["tries"]*3);
					}

					//get timing to use as rules
					$time_start=intval($_GET["time_start"]);
					$time_send=intval($_GET["time_send"]);

					if (($time_fromload)<=1000){
						//They posted in 1 second so its fake!
						$score=$score-30;
					}else{
						$score=$score+4;
					}

					if (($time_fromload)>=(1000*(60*15))){
						//They posted longer then 15 minutes so its not good
						$score=$score-12;
					}

					$queryx=sqdb_query("UPDATE attempts SET score='$score', time_fromload='$time_fromload' WHERE id_long='$session_key'");

					echo "good";
				}else{
					echo "error";
					$queryx=sqdb_query("UPDATE attempts SET score='5' WHERE id_long='$session_key'");
				}
			}else{
				echo "error";
				$queryx=sqdb_query("UPDATE attempts SET score='5' WHERE id_long='$session_key'");
			}

		}else{
			echo "error";
			$queryx=sqdb_query("UPDATE attempts SET score='5' WHERE id_long='$session_key'");
		}
	}else{
		echo "error";
		$queryx=sqdb_query("UPDATE attempts SET score='5' WHERE id_long='$session_key'");
	}

	$deleteold=sqdb_query("DELETE FROM puzzle WHERE id_long='$session_key'");
}

//Generate qustion based on key
if ($_GET["action"]=="question"){
	header('Content-Type: application/javascript');
	if ($session_key==false){
		$key=codegenerate("60","normal");
		$date=time();
		$session_key=$key;
		$resulta=sqdb_query("INSERT INTO attempts(id_long,date,ip) VALUES('$key', '$date', '$session_userip')");
	}
	$deleteold=sqdb_query("DELETE FROM puzzle WHERE id_long='$session_key'");

	//Generate question now! This can be intense as we are generating images also!
	$photo=array();
	$key=array();

	$photo["1"]=random_pic();
	$key["1"]=codegenerate("40","normal");
	$photo["2"]=random_pic();
	$key["2"]=codegenerate("40","normal");
	$photo["3"]=random_pic();
	$key["3"]=codegenerate("40","normal");
	$photo["4"]=random_pic();
	$key["4"]=codegenerate("40","normal");
	$photo["5"]=random_pic();
	$key["5"]=codegenerate("40","normal");

	$master_id=mt_rand(1,5);
	$master_key=$key["".$master_id.""];
	$master_photo=$photo["".$master_id.""];

	//save solution
	$date=time();
	$resulta=sqdb_query("INSERT INTO puzzle(id_long, date, solution) VALUES('$session_key', '$date', '$master_key')");

	//Return box
	echo "nh_visitorcheck_loadquestion({\"key\": \"".$session_key."\", \"question\": \"".system_scaleimage_random($master_photo,true)."\"";

	echo ", \"choice_1_image\": \"".system_scaleimage_random($photo["1"],false)."\", \"choice_1_key\": \"".$key["1"]."\"";
	echo ", \"choice_2_image\": \"".system_scaleimage_random($photo["2"],false)."\", \"choice_2_key\": \"".$key["2"]."\"";
	echo ", \"choice_3_image\": \"".system_scaleimage_random($photo["3"],false)."\", \"choice_3_key\": \"".$key["3"]."\"";
	echo ", \"choice_4_image\": \"".system_scaleimage_random($photo["4"],false)."\", \"choice_4_key\": \"".$key["4"]."\"";
	echo ", \"choice_5_image\": \"".system_scaleimage_random($photo["5"],false)."\", \"choice_5_key\": \"".$key["5"]."\"";

	echo "})";
}
